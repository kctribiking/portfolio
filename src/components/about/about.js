import * as React from 'react'
import Image from 'next/image'
import profile from '../../images/profile.jpg'

export default function About() {
  return (
    <div className='about-section' id='about-setion'>
      <div className='container-fluid'>
      <h1>About Me</h1>
        <div className='row'>
          <div className='col-md-7'>
            <p>Hello! My name is Tribikram and I enjoy creating things that live on the internet. My interest in web development started back in 2019 when I decided to develop my own webiste using free templete — turns out hacking together a custom buttons and texts taught me a lot about HTML & CSS!</p>
            <ul className='about-info mt-4 px-md-0 px-2'>
              <li className='d-flex'>
                <span>Name:</span> 
                <span>Tribikram Kc</span> 
              </li>
              <li className='d-flex'>
                <span>Date of birth:</span> 
                <span>05 june 1999</span> 
              </li>
              <li className='d-flex'>
                <span>Address:</span> 
                <span>Gulmi, Nepal</span> 
              </li>
              <li className='d-flex'>
                <span>Email:</span> 
                <span>kctribikram7@gmail.com</span> 
              </li>
              <li className='d-flex'>
                <span>Phone:</span> 
                <span>+977 9867256767</span> 
              </li>
            </ul>
            <div className='cv-btn'>
              <a href='' download className='cv'>Download CV</a>
            </div>
          </div>
          <div className='col-md-5'>
            <div className='profile-image'>
              <Image className='profile-img' src={profile} priority alt='Profile'/>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
