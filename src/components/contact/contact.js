import React from 'react'

export default function contact() {
  return (
    <>
        <div className='contact-section'>
            <div className='container-fluid'>
                <div className='row'>
                    <div className='col-md-12 contact-desc'>
                        <h1>Get In Touch</h1>
                        <p>Although I’m not currently looking for any new opportunities, my inbox is always open. Whether you have a question or just want to say hi, I’ll try my best to get back to you!</p>
                        <a href='mailto:kctribikram7@gmail.com' target="_blank" rel="noreferrer">Say Hello</a>
                    </div>
                </div>
            </div>        
        </div>
    </>
  )
}
