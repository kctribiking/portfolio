import React from 'react'
import Link from 'next/link'

export default function navbar() {
  return (
    <>
        <div className='navbar-section'>
            <div className='navbar'>
                <div className='navbar-title'>
                    <a href='/'><h1>Tribikram</h1></a> 
                </div>
                <ul className='navbar-lists'>
                    <li>
                       <Link href='/'>Home</Link> 
                    </li>
                    <li>
                    <Link href='#about-setion'>About</Link>
                    </li>
                    <li>
                        <Link href='#about-setion'>Resume</Link>
                    </li>
                    <li>
                        <Link href='#project-section'>Projects</Link>
                    </li>
                    <li>
                        <Link href='/'>Contact</Link>
                    </li>
                </ul>
            </div>
        </div>
    </>
  )
}
