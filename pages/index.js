import Head from 'next/head'
import dynamic from 'next/dynamic'
import Lottie from 'lottie-react'
import downArrow from '../src/lottie/down.json'

const Navbar = dynamic(() => import('../src/components/navbar/navbar'))
const About = dynamic(() => import('../src/components/about/about'))
const Projects = dynamic(() => import('../src/components/projects/projects'))
const Contact = dynamic(()=>import('../src/components/contact/contact'))

export default function Home() {
  const style = {
    height: 150,
  };
  return (
    <>
    <Head>
    <title>Kctribikram</title>
        <meta
            name = "description"
            content = "Kctribikram"
          />
          <meta
            name = "keywords"
            content = "Kctribikram"
          />
          <meta
            name="meta keywords"
            content="Kctribikram"
          />
          <meta
            name="og:image"
            content="Kctribikram"
          />
    </Head>
    <Navbar />
    <div className='banner-section'>
      <span className='home-banner'></span>
      <div className='content'>
        <h1><span className='top-content'>HEY! I AM</span><br/><span className='middle-text'>Tribikram Kc</span><br/>I'm a <span className='profession'>Frontend Developer</span></h1>
        <Lottie 
          animationData={downArrow}
          style={style}
        />
      </div>
    </div>
    <About />
    <Projects />
    <Contact />
    </>
  )
}
